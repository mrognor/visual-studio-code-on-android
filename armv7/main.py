from kivy.core.clipboard import Clipboard
from kivy.uix.boxlayout import BoxLayout
from kivy.app import App
import io
import zipfile
import requests
import webbrowser


class MainWindow(BoxLayout):
    text_to_copy = ""

    def copy(self):
        Clipboard.copy(self.text_to_copy)

    def hint(self):
        webbrowser.open_new("https://app.gitbook.com/@timurlego1/s/visual-studio-on-android/")

    def browser(self):
        webbrowser.open_new("http://localhost:8080")

class DiffApp(App):

    def build(self):
        return MainWindow()

    def on_start(self):
        try:
            zip_file = requests.get("https://gitlab.com/mrognor/visual-studio-code-on-android/-/archive/master/visual-studio-code-on-android-master.zip?path=text-to-android-app")
            with zip_file, zipfile.ZipFile(io.BytesIO(zip_file.content)) as archive:
                archive.extractall('output')
            text_file = open("output/visual-studio-code-on-android-master-text-to-android-app/text-to-android-app/text-to-app.txt")

            text_from_file = text_file.read().splitlines()
            count = len(text_from_file)

            text_to_label = ""

            for i in range(count-1):
                text_to_label += text_from_file[i] + "\n"

            text_to_copy = text_from_file[count-1]

        except:
            try:
                text_file = open("output/visualstudiocodeonandroid-master-text-to-android-app/text-to-android-app/text-to-app.txt")

                text_from_file = text_file.read().splitlines()
                count = len(text_from_file)

                text_to_label = ""

                for i in range(count-1):
                    text_to_label += text_from_file[i] + "\n"

                text_to_copy = text_from_file[count-1]
            except:
                text_to_label = "Sorry, you don't have internet access at the moment and I can't download the manual, but you still won't be able to download visual studio code)"
                text_to_copy = "Sorry, you don't have internet access at the moment and I can't download the manual, but you still won't be able to download visual studio code)"

        print(text_to_label)
        self.root.label_var.text = text_to_label
        self.root.text_to_copy = text_to_copy


if __name__ == "__main__":
    DiffApp().run()
